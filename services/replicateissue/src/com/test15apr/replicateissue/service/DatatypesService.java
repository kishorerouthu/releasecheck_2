/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/

package com.test15apr.replicateissue.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.test15apr.replicateissue.*;

/**
 * Service object for domain model class Datatypes.
 * @see com.test15apr.replicateissue.Datatypes
 */

public interface DatatypesService {
   /**
	 * Creates a new datatypes.
	 * 
	 * @param created
	 *            The information of the created datatypes.
	 * @return The created datatypes.
	 */
	public Datatypes create(Datatypes created);

	/**
	 * Deletes a datatypes.
	 * 
	 * @param datatypesId
	 *            The id of the deleted datatypes.
	 * @return The deleted datatypes.
	 * @throws EntityNotFoundException
	 *             if no datatypes is found with the given id.
	 */
	public Datatypes delete(Integer datatypesId) throws EntityNotFoundException;

	/**
	 * Finds all datatypess.
	 * 
	 * @return A list of datatypess.
	 */
	public Page<Datatypes> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Datatypes> findAll(Pageable pageable);
	
	/**
	 * Finds datatypes by id.
	 * 
	 * @param id
	 *            The id of the wanted datatypes.
	 * @return The found datatypes. If no datatypes is found, this method returns
	 *         null.
	 */
	public Datatypes findById(Integer id) throws
	 EntityNotFoundException;
	/**
	 * Updates the information of a datatypes.
	 * 
	 * @param updated
	 *            The information of the updated datatypes.
	 * @return The updated datatypes.
	 * @throws EntityNotFoundException
	 *             if no datatypes is found with given id.
	 */
	public Datatypes update(Datatypes updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the datatypess in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the datatypes.
	 */

	public long countAll();


    public Page<Datatypes> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

