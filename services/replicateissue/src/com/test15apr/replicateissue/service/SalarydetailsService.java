/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/

package com.test15apr.replicateissue.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.test15apr.replicateissue.*;

/**
 * Service object for domain model class Salarydetails.
 * @see com.test15apr.replicateissue.Salarydetails
 */

public interface SalarydetailsService {
   /**
	 * Creates a new salarydetails.
	 * 
	 * @param created
	 *            The information of the created salarydetails.
	 * @return The created salarydetails.
	 */
	public Salarydetails create(Salarydetails created);

	/**
	 * Deletes a salarydetails.
	 * 
	 * @param salarydetailsId
	 *            The id of the deleted salarydetails.
	 * @return The deleted salarydetails.
	 * @throws EntityNotFoundException
	 *             if no salarydetails is found with the given id.
	 */
	public Salarydetails delete(Integer salarydetailsId) throws EntityNotFoundException;

	/**
	 * Finds all salarydetailss.
	 * 
	 * @return A list of salarydetailss.
	 */
	public Page<Salarydetails> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Salarydetails> findAll(Pageable pageable);
	
	/**
	 * Finds salarydetails by id.
	 * 
	 * @param id
	 *            The id of the wanted salarydetails.
	 * @return The found salarydetails. If no salarydetails is found, this method returns
	 *         null.
	 */
	public Salarydetails findById(Integer id) throws
	 EntityNotFoundException;
	/**
	 * Updates the information of a salarydetails.
	 * 
	 * @param updated
	 *            The information of the updated salarydetails.
	 * @return The updated salarydetails.
	 * @throws EntityNotFoundException
	 *             if no salarydetails is found with given id.
	 */
	public Salarydetails update(Salarydetails updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the salarydetailss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the salarydetails.
	 */

	public long countAll();


    public Page<Salarydetails> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

