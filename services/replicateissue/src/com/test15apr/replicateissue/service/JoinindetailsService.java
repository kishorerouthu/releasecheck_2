/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/

package com.test15apr.replicateissue.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/




import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;

import com.test15apr.replicateissue.*;

/**
 * Service object for domain model class Joinindetails.
 * @see com.test15apr.replicateissue.Joinindetails
 */

public interface JoinindetailsService {
   /**
	 * Creates a new joinindetails.
	 * 
	 * @param created
	 *            The information of the created joinindetails.
	 * @return The created joinindetails.
	 */
	public Joinindetails create(Joinindetails created);

	/**
	 * Deletes a joinindetails.
	 * 
	 * @param joinindetailsId
	 *            The id of the deleted joinindetails.
	 * @return The deleted joinindetails.
	 * @throws EntityNotFoundException
	 *             if no joinindetails is found with the given id.
	 */
	public Joinindetails delete(Integer joinindetailsId) throws EntityNotFoundException;

	/**
	 * Finds all joinindetailss.
	 * 
	 * @return A list of joinindetailss.
	 */
	public Page<Joinindetails> findAll(QueryFilter[] queryFilters, Pageable pageable);
	
	public Page<Joinindetails> findAll(Pageable pageable);
	
	/**
	 * Finds joinindetails by id.
	 * 
	 * @param id
	 *            The id of the wanted joinindetails.
	 * @return The found joinindetails. If no joinindetails is found, this method returns
	 *         null.
	 */
	public Joinindetails findById(Integer id) throws
	 EntityNotFoundException;
	/**
	 * Updates the information of a joinindetails.
	 * 
	 * @param updated
	 *            The information of the updated joinindetails.
	 * @return The updated joinindetails.
	 * @throws EntityNotFoundException
	 *             if no joinindetails is found with given id.
	 */
	public Joinindetails update(Joinindetails updated) throws EntityNotFoundException;

	/**
	 * Retrieve the total count of the joinindetailss in the repository.
	 * 
	 * @param None
	 *            .
	 * @return The count of the joinindetails.
	 */

	public long countAll();


    public Page<Joinindetails> findAssociatedValues(Object value, String entityName, String key,  Pageable pageable);


}

