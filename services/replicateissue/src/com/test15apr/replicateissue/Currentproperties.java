/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/

package com.test15apr.replicateissue;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;



/**
 * Currentproperties generated by hbm2java
 */
@Entity
@Table(name="`currentproperties`"
    ,schema="public"
)

public class Currentproperties  implements java.io.Serializable {

    
    private Integer iduser;
    
    private String currentusername;
    
    private String currentuseridcol;
    
    private Date currenttime;
    
    private Date currentdate;

    public Currentproperties() {
    }


    @Id @GeneratedValue(strategy=IDENTITY)
    

    @Column(name="`iduser`", precision=10)
    public Integer getIduser() {
        return this.iduser;
    }
    
    public void setIduser(Integer iduser) {
        this.iduser = iduser;
    }

    

    @Column(name="`currentusername`")
    public String getCurrentusername() {
        return this.currentusername;
    }
    
    public void setCurrentusername(String currentusername) {
        this.currentusername = currentusername;
    }

    

    @Column(name="`currentuseridcol`")
    public String getCurrentuseridcol() {
        return this.currentuseridcol;
    }
    
    public void setCurrentuseridcol(String currentuseridcol) {
        this.currentuseridcol = currentuseridcol;
    }

    @Temporal(TemporalType.TIME)

    @Column(name="`currenttime`", length=15)
    public Date getCurrenttime() {
        return this.currenttime;
    }
    
    public void setCurrenttime(Date currenttime) {
        this.currenttime = currenttime;
    }

    @Temporal(TemporalType.DATE)

    @Column(name="`currentdate`", length=13)
    public Date getCurrentdate() {
        return this.currentdate;
    }
    
    public void setCurrentdate(Date currentdate) {
        this.currentdate = currentdate;
    }





    public boolean equals(Object o) {
         if (this == o) return true;
		 if ( (o == null )) return false;
		 if ( !(o instanceof Currentproperties) )
		    return false;

		 Currentproperties that = (Currentproperties) o;

		 return ( (this.getIduser()==that.getIduser()) || ( this.getIduser()!=null && that.getIduser()!=null && this.getIduser().equals(that.getIduser()) ) );
    }

    public int hashCode() {
         int result = 17;

         result = 37 * result + ( getIduser() == null ? 0 : this.getIduser().hashCode() );

         return result;
    }


}

