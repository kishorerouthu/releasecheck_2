/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/

package com.test15apr.wmstudio;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import javax.persistence.PrimaryKeyJoinColumn;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;


import java.util.Arrays;

import javax.persistence.Transient;
import javax.persistence.CascadeType;
import javax.persistence.UniqueConstraint;



/**
 * JoiningDetails generated by hbm2java
 */
@Entity
@Table(name="`JOINING DETAILS`"
    ,schema="VIKAS"
)

public class JoiningDetails  implements java.io.Serializable {

    
    private Integer userId;
    
    
    @Type(type="DateTime")
    private LocalDateTime dateJoined;
    
    private Double startupSal;
    
    private Float hike;
    
    
    @Type(type="DateTime")
    private LocalDateTime timecol;
    
    private Date timestampCol;
    
    
    @Type(type="DateTime")
    private LocalDateTime workTime;
    
    private Byte byteCol;
    
    private String field;
    
    private Short shortCol;
    
    private SalaryDetails salaryDetails;
    
    private UserDetails userDetails;
    
    private Users users;

    public JoiningDetails() {
    }


    @GenericGenerator(name="generator", strategy="foreign", parameters=@Parameter(name="property", value="users"))@Id @GeneratedValue(generator="generator")
    

    @Column(name="`USER ID`", precision=10)
    public Integer getUserId() {
        return this.userId;
    }
    
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    

    @Column(name="`DATE JOINED`")
    public LocalDateTime getDateJoined() {
        return this.dateJoined;
    }
    
    public void setDateJoined(LocalDateTime dateJoined) {
        this.dateJoined = dateJoined;
    }

    

    @Column(name="`STARTUP SAL`", scale=8)
    public Double getStartupSal() {
        return this.startupSal;
    }
    
    public void setStartupSal(Double startupSal) {
        this.startupSal = startupSal;
    }

    

    @Column(name="`HIKE`", precision=6, scale=4)
    public Float getHike() {
        return this.hike;
    }
    
    public void setHike(Float hike) {
        this.hike = hike;
    }

    

    @Column(name="`TIMECOL`")
    public LocalDateTime getTimecol() {
        return this.timecol;
    }
    
    public void setTimecol(LocalDateTime timecol) {
        this.timecol = timecol;
    }

    @Temporal(TemporalType.TIMESTAMP)

    @Column(name="`TIMESTAMP COL`", length=11)
    public Date getTimestampCol() {
        return this.timestampCol;
    }
    
    public void setTimestampCol(Date timestampCol) {
        this.timestampCol = timestampCol;
    }

    

    @Column(name="`WORK TIME`")
    public LocalDateTime getWorkTime() {
        return this.workTime;
    }
    
    public void setWorkTime(LocalDateTime workTime) {
        this.workTime = workTime;
    }

    

    @Column(name="`BYTE COL`", precision=2)
    public Byte getByteCol() {
        return this.byteCol;
    }
    
    public void setByteCol(Byte byteCol) {
        this.byteCol = byteCol;
    }

    

    @Column(name="`FIELD`", length=20)
    public String getField() {
        return this.field;
    }
    
    public void setField(String field) {
        this.field = field;
    }

    

    @Column(name="`SHORT COL`", precision=3)
    public Short getShortCol() {
        return this.shortCol;
    }
    
    public void setShortCol(Short shortCol) {
        this.shortCol = shortCol;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`SAL ID`")
    public SalaryDetails getSalaryDetails() {
        return this.salaryDetails;
    }
    
    public void setSalaryDetails(SalaryDetails salaryDetails) {
        this.salaryDetails = salaryDetails;
    }

    @ManyToOne(fetch=FetchType.EAGER)
    @JoinColumn(name="`USER DETA ID`")
    public UserDetails getUserDetails() {
        return this.userDetails;
    }
    
    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    @OneToOne(fetch=FetchType.EAGER) @PrimaryKeyJoinColumn
    public Users getUsers() {
        return this.users;
    }
    
    public void setUsers(Users users) {
        this.users = users;
    }





    public boolean equals(Object o) {
         if (this == o) return true;
		 if ( (o == null )) return false;
		 if ( !(o instanceof JoiningDetails) )
		    return false;

		 JoiningDetails that = (JoiningDetails) o;

		 return ( (this.getUserId()==that.getUserId()) || ( this.getUserId()!=null && that.getUserId()!=null && this.getUserId().equals(that.getUserId()) ) );
    }

    public int hashCode() {
         int result = 17;

         result = 37 * result + ( getUserId() == null ? 0 : this.getUserId().hashCode() );

         return result;
    }


}

