/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.test15apr.wmstudio.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/
import com.test15apr.wmstudio.service.CompKeysMulService;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import org.joda.time.LocalDateTime;
import org.springframework.web.bind.annotation.RequestBody;
import java.io.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.hibernate.TypeMismatchException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.util.WMMultipartUtils;
import com.wavemaker.runtime.util.WMRuntimeUtils;
import com.wavemaker.runtime.file.model.DownloadResponse;
import com.wordnik.swagger.annotations.*;
import com.test15apr.wmstudio.*;
import com.test15apr.wmstudio.service.*;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;

/**
 * Controller object for domain model class CompKeysMul.
 * @see com.test15apr.wmstudio.CompKeysMul
 */
@RestController(value = "WMSTUDIO.CompKeysMulController")
@RequestMapping("/WMSTUDIO/CompKeysMul")
@Api(description = "Exposes APIs to work with CompKeysMul resource.", value = "CompKeysMulController")
public class CompKeysMulController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompKeysMulController.class);

    @Autowired
    @Qualifier("WMSTUDIO.CompKeysMulService")
    private CompKeysMulService compKeysMulService;

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @ApiOperation(value = "Returns the list of CompKeysMul instances matching the search criteria.")
    public Page<CompKeysMul> findCompKeysMuls(Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering CompKeysMuls list");
        return compKeysMulService.findAll(queryFilters, pageable);
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ApiOperation(value = "Returns the list of CompKeysMul instances.")
    public Page<CompKeysMul> getCompKeysMuls(Pageable pageable) {
        LOGGER.debug("Rendering CompKeysMuls list");
        return compKeysMulService.findAll(pageable);
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT, consumes = { "multipart/form-data" })
    @ApiOperation(value = "Updates the CompKeysMul instance associated with the given composite-id.")
    public CompKeysMul editCompKeysMulAndMultiparts(@RequestParam("time") LocalDateTime time, @RequestParam("byte_") Byte byte_, @RequestParam("datecol") LocalDateTime datecol, @RequestParam("bigint") BigInteger bigint, @RequestParam("datetime") LocalDateTime datetime, @RequestParam("stringcol") String stringcol, @RequestParam("shortcol") Short shortcol, @RequestParam("double_") Double double_, @RequestParam("longcol") Long longcol, @RequestParam("float_") Float float_, @RequestParam("id") Integer id, @RequestParam("timestamp") Date timestamp, @RequestParam("bigdeci") BigDecimal bigdeci, MultipartHttpServletRequest multipartHttpServletRequest) throws EntityNotFoundException {
        CompKeysMulId temp = new CompKeysMulId();
        temp.setTime(time);
        temp.setByte_(byte_);
        temp.setDatecol(datecol);
        temp.setBigint(bigint);
        temp.setDatetime(datetime);
        temp.setStringcol(stringcol);
        temp.setShortcol(shortcol);
        temp.setDouble_(double_);
        temp.setLongcol(longcol);
        temp.setFloat_(float_);
        temp.setId(id);
        temp.setTimestamp(timestamp);
        temp.setBigdeci(bigdeci);
        CompKeysMul newcompkeysmul = WMMultipartUtils.toObject(multipartHttpServletRequest, CompKeysMul.class, "WMSTUDIO");
        CompKeysMul oldcompkeysmul = compKeysMulService.findById(temp);
        WMMultipartUtils.updateLobsContent(oldcompkeysmul, newcompkeysmul);
        compKeysMulService.delete(temp);
        CompKeysMul instance = this.createCompKeysMul(newcompkeysmul);
        LOGGER.debug("CompKeysMul details with id is updated: {}", instance);
        return instance;
    }

    /**
	 * This setter method should only be used by unit tests
	 * 
	 * @param service
	 */
    protected void setCompKeysMulService(CompKeysMulService service) {
        this.compKeysMulService = service;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new CompKeysMul instance.")
    public CompKeysMul createCompKeysMul(@RequestBody CompKeysMul instance) {
        LOGGER.debug("Create CompKeysMul with information: {}", instance);
        instance = compKeysMulService.create(instance);
        LOGGER.debug("Created CompKeysMul with information: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/", method = RequestMethod.POST, consumes = { "multipart/form-data" })
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Creates a new CompKeysMul instance.")
    public CompKeysMul createCompKeysMul(MultipartHttpServletRequest multipartHttpServletRequest) {
        CompKeysMul compkeysmul = WMMultipartUtils.toObject(multipartHttpServletRequest, CompKeysMul.class, "WMSTUDIO");
        LOGGER.debug("Creating a new compkeysmul with information: {}", compkeysmul);
        return compKeysMulService.create(compkeysmul);
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the CompKeysMul instance associated with the given composite-id.")
    public CompKeysMul getCompKeysMul(@RequestParam(value = "time", required = true) LocalDateTime time, @RequestParam(value = "byte_", required = true) Byte byte_, @RequestParam(value = "datecol", required = true) LocalDateTime datecol, @RequestParam(value = "bigint", required = true) BigInteger bigint, @RequestParam(value = "datetime", required = true) LocalDateTime datetime, @RequestParam(value = "stringcol", required = true) String stringcol, @RequestParam(value = "shortcol", required = true) Short shortcol, @RequestParam(value = "double_", required = true) Double double_, @RequestParam(value = "longcol", required = true) Long longcol, @RequestParam(value = "float_", required = true) Float float_, @RequestParam(value = "id", required = true) Integer id, @RequestParam(value = "timestamp", required = true) Date timestamp, @RequestParam(value = "bigdeci", required = true) BigDecimal bigdeci) throws EntityNotFoundException {
        CompKeysMulId temp = new CompKeysMulId();
        temp.setTime(time);
        temp.setByte_(byte_);
        temp.setDatecol(datecol);
        temp.setBigint(bigint);
        temp.setDatetime(datetime);
        temp.setStringcol(stringcol);
        temp.setShortcol(shortcol);
        temp.setDouble_(double_);
        temp.setLongcol(longcol);
        temp.setFloat_(float_);
        temp.setId(id);
        temp.setTimestamp(timestamp);
        temp.setBigdeci(bigdeci);
        LOGGER.debug("Getting CompKeysMul with id: {}", temp);
        CompKeysMul instance = compKeysMulService.findById(temp);
        LOGGER.debug("CompKeysMul details with id: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Updates the CompKeysMul instance associated with the given composite-id.")
    public CompKeysMul editCompKeysMul(@RequestParam(value = "time", required = true) LocalDateTime time, @RequestParam(value = "byte_", required = true) Byte byte_, @RequestParam(value = "datecol", required = true) LocalDateTime datecol, @RequestParam(value = "bigint", required = true) BigInteger bigint, @RequestParam(value = "datetime", required = true) LocalDateTime datetime, @RequestParam(value = "stringcol", required = true) String stringcol, @RequestParam(value = "shortcol", required = true) Short shortcol, @RequestParam(value = "double_", required = true) Double double_, @RequestParam(value = "longcol", required = true) Long longcol, @RequestParam(value = "float_", required = true) Float float_, @RequestParam(value = "id", required = true) Integer id, @RequestParam(value = "timestamp", required = true) Date timestamp, @RequestParam(value = "bigdeci", required = true) BigDecimal bigdeci, @RequestBody CompKeysMul instance) throws EntityNotFoundException {
        CompKeysMulId temp = new CompKeysMulId();
        temp.setTime(time);
        temp.setByte_(byte_);
        temp.setDatecol(datecol);
        temp.setBigint(bigint);
        temp.setDatetime(datetime);
        temp.setStringcol(stringcol);
        temp.setShortcol(shortcol);
        temp.setDouble_(double_);
        temp.setLongcol(longcol);
        temp.setFloat_(float_);
        temp.setId(id);
        temp.setTimestamp(timestamp);
        temp.setBigdeci(bigdeci);
        compKeysMulService.delete(temp);
        instance = compKeysMulService.create(instance);
        LOGGER.debug("CompKeysMul details with id is updated: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.POST, consumes = { "multipart/form-data" })
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Create the CompKeysMul instance associated with the given composite-id.This API should be used when CompKeysMul instance fields that require multipart data.")
    public CompKeysMul editCompKeysMul(@RequestParam(value = "time", required = true) LocalDateTime time, @RequestParam(value = "byte_", required = true) Byte byte_, @RequestParam(value = "datecol", required = true) LocalDateTime datecol, @RequestParam(value = "bigint", required = true) BigInteger bigint, @RequestParam(value = "datetime", required = true) LocalDateTime datetime, @RequestParam(value = "stringcol", required = true) String stringcol, @RequestParam(value = "shortcol", required = true) Short shortcol, @RequestParam(value = "double_", required = true) Double double_, @RequestParam(value = "longcol", required = true) Long longcol, @RequestParam(value = "float_", required = true) Float float_, @RequestParam(value = "id", required = true) Integer id, @RequestParam(value = "timestamp", required = true) Date timestamp, @RequestParam(value = "bigdeci", required = true) BigDecimal bigdeci, MultipartHttpServletRequest multipartHttpServletRequest) throws EntityNotFoundException {
        CompKeysMulId temp = new CompKeysMulId();
        temp.setTime(time);
        temp.setByte_(byte_);
        temp.setDatecol(datecol);
        temp.setBigint(bigint);
        temp.setDatetime(datetime);
        temp.setStringcol(stringcol);
        temp.setShortcol(shortcol);
        temp.setDouble_(double_);
        temp.setLongcol(longcol);
        temp.setFloat_(float_);
        temp.setId(id);
        temp.setTimestamp(timestamp);
        temp.setBigdeci(bigdeci);
        compKeysMulService.delete(temp);
        CompKeysMul instance = this.createCompKeysMul(multipartHttpServletRequest);
        LOGGER.debug("CompKeysMul details with id is updated: {}", instance);
        return instance;
    }

    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Deletes the CompKeysMul instance associated with the given composite-id.")
    public boolean deleteCompKeysMul(@RequestParam(value = "time", required = true) LocalDateTime time, @RequestParam(value = "byte_", required = true) Byte byte_, @RequestParam(value = "datecol", required = true) LocalDateTime datecol, @RequestParam(value = "bigint", required = true) BigInteger bigint, @RequestParam(value = "datetime", required = true) LocalDateTime datetime, @RequestParam(value = "stringcol", required = true) String stringcol, @RequestParam(value = "shortcol", required = true) Short shortcol, @RequestParam(value = "double_", required = true) Double double_, @RequestParam(value = "longcol", required = true) Long longcol, @RequestParam(value = "float_", required = true) Float float_, @RequestParam(value = "id", required = true) Integer id, @RequestParam(value = "timestamp", required = true) Date timestamp, @RequestParam(value = "bigdeci", required = true) BigDecimal bigdeci) throws EntityNotFoundException {
        CompKeysMulId temp = new CompKeysMulId();
        temp.setTime(time);
        temp.setByte_(byte_);
        temp.setDatecol(datecol);
        temp.setBigint(bigint);
        temp.setDatetime(datetime);
        temp.setStringcol(stringcol);
        temp.setShortcol(shortcol);
        temp.setDouble_(double_);
        temp.setLongcol(longcol);
        temp.setFloat_(float_);
        temp.setId(id);
        temp.setTimestamp(timestamp);
        temp.setBigdeci(bigdeci);
        LOGGER.debug("Deleting CompKeysMul with id: {}", temp);
        CompKeysMul deleted = compKeysMulService.delete(temp);
        return deleted != null;
    }

    @RequestMapping(value = "/composite-id/content/{fieldName}", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Retrieves content for the given BLOB field in CompKeysMul instance associated with the given composite-id.")
    public void getCompKeysMulBLOBContent(@RequestParam(value = "time", required = true) LocalDateTime time, @RequestParam(value = "byte_", required = true) Byte byte_, @RequestParam(value = "datecol", required = true) LocalDateTime datecol, @RequestParam(value = "bigint", required = true) BigInteger bigint, @RequestParam(value = "datetime", required = true) LocalDateTime datetime, @RequestParam(value = "stringcol", required = true) String stringcol, @RequestParam(value = "shortcol", required = true) Short shortcol, @RequestParam(value = "double_", required = true) Double double_, @RequestParam(value = "longcol", required = true) Long longcol, @RequestParam(value = "float_", required = true) Float float_, @RequestParam(value = "id", required = true) Integer id, @RequestParam(value = "timestamp", required = true) Date timestamp, @RequestParam(value = "bigdeci", required = true) BigDecimal bigdeci, @PathVariable(value = "fieldName") String fieldName, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws EntityNotFoundException {
        LOGGER.debug("Retrieves content for the given BLOB field {} in CompKeysMul instance", fieldName);
        if (!WMRuntimeUtils.isLob(CompKeysMul.class, fieldName)) {
            throw new TypeMismatchException("Given field " + fieldName + " is not a valid BLOB type");
        }
        CompKeysMulId temp = new CompKeysMulId();
        temp.setTime(time);
        temp.setByte_(byte_);
        temp.setDatecol(datecol);
        temp.setBigint(bigint);
        temp.setDatetime(datetime);
        temp.setStringcol(stringcol);
        temp.setShortcol(shortcol);
        temp.setDouble_(double_);
        temp.setLongcol(longcol);
        temp.setFloat_(float_);
        temp.setId(id);
        temp.setTimestamp(timestamp);
        temp.setBigdeci(bigdeci);
        CompKeysMul instance = compKeysMulService.findById(temp);
        WMMultipartUtils.buildHttpResponseForBlob(instance, fieldName, httpServletRequest, httpServletResponse);
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    @ApiOperation(value = "Returns the total count of CompKeysMul instances.")
    public Long countAllCompKeysMuls() {
        LOGGER.debug("counting CompKeysMuls");
        Long count = compKeysMulService.countAll();
        return count;
    }
}
